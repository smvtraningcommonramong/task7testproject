﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    /// <summary>
    /// Class that holds the properties of a OpenCV pixel value.
    /// </summary>
    /// <remarks>It is used to store pixel values from OpenCV to be compared with the results of GetPixel implementation.</remarks>
    /// <typeparam name="T">Pixel type.</typeparam>
    public struct OCVTestPixel<T>
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public T[] Values { get; set; }
    }

}
