﻿using System;
using System.Drawing;
using ImageContainer;
using Xunit;

namespace UnitTests.ImageContainerTests
{
    public class ImageManagementTests
    {

        [Fact]
        public void ShouldSetToPixelVal()
        {
            //Arrange
            byte[] exPixel8UC3 = new byte[] { 64, 128, 192 };
            ushort[] exPixel16UC3 = new ushort[] { 20000, 45678, 10000 };
            float[] exPixel32FC3 = new float[] { 0.5f, 0.7f, 1 };
            var actualImage = new SMVImage();

            //Act and Assert U8VEC3
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.U8C3, new PixelVal(exPixel8UC3));
            var expectedImage = new SMVImage(new Size(3, 3), ImageTypes.U8C3, new PixelVal(new byte[] { 0, 0, 0 }));
            expectedImage.SetTo(exPixel8UC3);
            TestHelper.BasicCompare(expectedImage, actualImage);

            //Act and Assert U8
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.U8C1, new PixelVal(exPixel8UC3[0]));
            expectedImage = new SMVImage(new Size(3, 3), ImageTypes.U8C1, new PixelVal(new byte[] { 0 }));
            expectedImage.SetTo(exPixel8UC3[0]);
            TestHelper.BasicCompare(expectedImage, actualImage);

            //Act and Assert 16VEC3
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.U16C3, new PixelVal(exPixel16UC3));
            expectedImage = new SMVImage(new Size(3, 3), ImageTypes.U16C3, new PixelVal(new ushort[] { 0, 0, 0 }));
            expectedImage.SetTo(exPixel8UC3);
            TestHelper.BasicCompare(expectedImage, actualImage);

            //Act and Assert U16
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.U16C3, new PixelVal(exPixel16UC3[0]));
            expectedImage = new SMVImage(new Size(3, 3), ImageTypes.U16C3, new PixelVal(new ushort[] { 0 }));
            expectedImage.SetTo(exPixel16UC3[0]);
            TestHelper.BasicCompare(expectedImage, actualImage);

            //Act and Assert F32CVEC3
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.F32C3, new PixelVal(exPixel32FC3));
            expectedImage = new SMVImage(new Size(3, 3), ImageTypes.F32C3, new PixelVal(new float[] { 0, 0, 0 }));
            expectedImage.SetTo(exPixel32FC3);
            TestHelper.BasicCompare(expectedImage, actualImage);

            //Act and Assert F32C
            actualImage = new SMVImage(new Size(3, 3), ImageTypes.F32C3, new PixelVal(exPixel32FC3[0]));
            expectedImage = new SMVImage(new Size(3, 3), ImageTypes.F32C3, new PixelVal(new float[] { 0 }));
            expectedImage.SetTo(exPixel32FC3);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void SetToShouldThrowArgumentException()
        {
            // Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(120, 130, 140));
            var pixelValObjToSet = new PixelVal(new double[] { 10.6, 20.2, 0.6 });
            var pixelvalArrayToSet = new byte[] { 10, 20 };

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetTo(pixelValObjToSet)); // Bad type of PixelType. Double vs byte
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetTo(pixelvalArrayToSet)); // Number of elems in array does not match channels in image
        }

        [Fact]
        public void ShouldCopyImage()
        {
            //Arrange
            var expectedImage = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(250,250,250));

            //Act
            var actualImage = expectedImage.CopyImageRef();

            //Assert
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void CopyImageReftShouldThrowArgumentNullException()
        {
            // Arrange
            SMVImage image = null;
            SMVImage image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => image.CloneImage());
            Assert.Throws<System.ArgumentNullException>(() => image2.CloneImage());
        }

        [Fact]
        public void ShouldCloneImage()
        {
            //Arrange
            var expectedImage = new SMVImage(new Size(400, 300), ImageTypes.F32C3, new PixelVal(0));

            //Act
            var actualImage = expectedImage.CloneImage();

            //Assert
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void ClonetShouldThrowArgumentNullException()
        {
            // Arrange
            SMVImage image = null;
            SMVImage image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => image.CloneImage());
            Assert.Throws<System.ArgumentNullException>(() => image2.CloneImage());
        }


        [Fact]
        public void ShouldSplitImageIntoChannels()
        {
            // Arrange
            var image = new SMVImage(new Size(156, 456), ImageTypes.U8C3, new PixelVal(120, 56, 10));
            int expectedNoChannels = 3;
            IImageContainer[] expectedChannels = new IImageContainer[]
            {
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(120)),
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(56)),
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(10))
            };

            // Act
            var actualChannels = image.Split();
            int actualNoChannels = actualChannels.Length;

            // Assert
            Assert.Equal(expectedNoChannels, actualNoChannels);
            for (int i = 0; i < expectedChannels.Length; i++)
            {
                Assert.NotNull(actualChannels[i]);
                TestHelper.BasicCompare(expectedChannels[i], actualChannels[i]);
            }
        }

        [Fact]
        public void SplitShouldThrowArgumentNullException()
        {
            // Arrange
            SMVImage image = null;
            SMVImage image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => image.Split());
            Assert.Throws<System.ArgumentNullException>(() => image2.Split());
        }

        [Theory]
        [InlineData(ImageTypes.U8C1, PixelTypes.U16, ImageTypes.U16C1, new double[] { 45 })]
        [InlineData(ImageTypes.U16C3, PixelTypes.U8, ImageTypes.U8C3, new double[] { 4556, 6523, 1299 })]
        public void ShouldConvertImageToDifferentPixelType(ImageTypes imageTypeOrig, PixelTypes destType, ImageTypes finalImgType, double[] origPixelVal)
        {
            // Arrange3
            var imageOrig = new SMVImage(new Size(156, 456), imageTypeOrig, new PixelVal(origPixelVal));
            var newImage = imageOrig.CloneImage();
            ImageTypes expectedImageType = finalImgType;

            // Act
            newImage.ConvertToType(destType);
            ImageTypes actualImageType = newImage.Type;

            // Assert
            Assert.Equal(expectedImageType, actualImageType);
            // Verify some values corresponding to the new type
            TestHelper.BasicCompare(imageOrig, newImage, 0.2f, 02f);
        }

        [Fact]
        public void ConvertToTypeShouldThrowArgumentNullException()
        {
            // Arrange
            SMVImage image = null;
            SMVImage image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => image.ConvertToType(PixelTypes.U16));
            Assert.Throws<System.ArgumentNullException>(() => image.ConvertToType(PixelTypes.U16)); // Empty image
        }

        [Theory]
        [InlineData(ImageTypes.U8C1, 30, new byte[] { 64 })]
        [InlineData(ImageTypes.U8C3, new byte[] { 30, 250, 35 }, new byte[] { 64, 128, 192 })]
        [InlineData(ImageTypes.U16C1, 10050, new ushort[] { 20000 })]
        [InlineData(ImageTypes.U16C3, new ushort[] { 10050, 4000, 65000 }, new ushort[] { 20000, 45678, 10000 })]
        [InlineData(ImageTypes.F32C1, 0.3f, new float[] { 0.5f })]
        [InlineData(ImageTypes.F32C3, new float[] { 0.3f, 0.3f, 0.3f }, new float[] { 0.5f, 0.7f, 1 })]
        public void ShouldSetToValue(ImageTypes imageType, dynamic inputArray, dynamic diferentValue)
        {
            //Arrange
            var expectedImage = new SMVImage(new Size(3, 3), imageType, new PixelVal(inputArray));
            var expectedPixel = new PixelVal(diferentValue);

            //Act
            var actualImage = new SMVImage(new Size(3, 3), imageType, new PixelVal(diferentValue));
            //Act
            if (expectedImage.Channels == 1)
            { expectedImage.SetTo(inputArray[0]); }
            else
            { expectedImage.SetTo(inputArray); }
           
            //Assert
            Assert.NotNull(expectedImage);
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }



        [Theory]
        [InlineData(ImageTypes.U8C1, 30, new byte[] { 64})]
        [InlineData(ImageTypes.U8C3, new byte[] { 30, 250, 35 }, new byte[] { 64, 128, 192 })]
        [InlineData(ImageTypes.U16C1, 10050, new ushort[] { 20000 })]
        [InlineData(ImageTypes.U16C3, new ushort[] { 10050, 4000, 65000 }, new ushort[] { 20000, 45678, 10000 })]
        [InlineData(ImageTypes.F32C1, 0.3f, new float[] { 0.5f })]
        [InlineData(ImageTypes.F32C3, new float[] { 0.3f, 0.3f, 0.3f }, new float[]{0.5f,0.7f,1})]
        public void ShouldSetPixelVal(ImageTypes imageType, dynamic inputArray, dynamic setValue)
        {
            //Arrange
            var expectedImage = new SMVImage(new Size(3, 3), imageType,new PixelVal(inputArray));
            var expectedPixel = new PixelVal(setValue);

            //Act
            if (expectedImage.Channels == 1)
            { expectedImage.SetPixel(3, 3, setValue[0]); }
            else
            { expectedImage.SetPixel(3, 3, setValue); }
            
            // Assert
            Assert.True(expectedPixel == expectedImage.GetPixel(3, 3));
        }

        [Fact]
        public void ShouldConvertToGrayScale()
        {
            //Arrange
            var expectedImage = new SMVImage();
            expectedImage.ReadImage("../../TestImages/lena.tiff", ImageModes.GrayScale);

            //Act
            var actualImage = new SMVImage();
            actualImage.ReadImage("../../TestImages/lena.tiff");
            actualImage.ConvertToGrayScale();

            //Assert
            Assert.NotNull(expectedImage);
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void ConvertToGrayScaleShouldThrowArgumentNullException()
        {
            // Arrange
            SMVImage image = null;
            SMVImage image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => image.ConvertToGrayScale());
            Assert.Throws<System.ArgumentNullException>(() => image.ConvertToGrayScale()); // Empty image
        }

        [Fact]
        public void ShouldReturnSubImage()
        {
            // Arrange
            var wholeImage = new SMVImage();
            wholeImage.ReadImage("../../TestImages/lenaU8C3.tiff");
            ImageTypes expectedImgType = ImageTypes.U8C3;
            Size expectedSize = new Size(200, 200);
            int expectedChannels = 3;
            long expectedRowSteps = 1536;

            // Act and Assert
            var actualImageROI = new SMVImage();
            actualImageROI.SubImage(200, 200, 400, 400);

            Assert.NotNull(wholeImage);
            Assert.NotNull(actualImageROI);
            Assert.Equal(expectedImgType, actualImageROI.Type);
            Assert.Equal(expectedChannels, actualImageROI.Channels);
            Assert.Equal(expectedSize, new Size(actualImageROI.Width, actualImageROI.Height));
            Assert.Equal(expectedRowSteps, actualImageROI.Steps);
            TestHelper.CheckRoiInImage(wholeImage, actualImageROI, new Rectangle(200, 200, 200, 200));
        }

        [Fact]
        public void ShouldReturnSubImageRectangle()
        {
            // Arrange
            var rectRoi = new Rectangle(200, 200, 200, 200);
            var wholeImage = new SMVImage();
            wholeImage.ReadImage("../../TestImages/lenaU8C3.tiff");
            ImageTypes expectedImgType = ImageTypes.U8C3;
            Size expectedSize = new Size(200, 200);
            int expectedChannels = 3;
            long expectedRowSteps = 1536;

            // Act and Assert
            var actualImageROI = new SMVImage();
            actualImageROI.SubImage(rectRoi);

            Assert.NotNull(wholeImage);
            Assert.NotNull(actualImageROI);
            Assert.Equal(expectedImgType, actualImageROI.Type);
            Assert.Equal(expectedChannels, actualImageROI.Channels);
            Assert.Equal(expectedSize, new Size(actualImageROI.Width, actualImageROI.Height));
            Assert.Equal(expectedRowSteps, actualImageROI.Steps);
            TestHelper.CheckRoiInImage(wholeImage, actualImageROI, rectRoi);
        }

        [Fact]
        public void SubImageShouldThrowArgumentOutOfRangeException()
        {
            //Arrange
            var image = new SMVImage(new Size(50, 50), ImageTypes.U8C3, 0);

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImage(-10, 10, 20, 20));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImage(new Rectangle(10, 10, 120, 120)));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImage(20, 20, 10, 10)); // Point 2 is smaller than point 1
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImageClone(-10, 10, 20, 20));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImageClone(new Rectangle(10, 10, 120, 120)));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SubImageClone(20, 20, 10, 10)); // Point 2 is smaller than point 1
        }

        [Fact]
        public void ShouldResizeImage()
        {
            //Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(0, 0, 0));
            var expectedImage = new SMVImage(new Size(200, 150), ImageTypes.U8C3, new PixelVal(0, 0, 0));

            //Act
            var actualImage = image.ResizeImage(200, 150);

            //Assert
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void ShouldResizeImageSize()
        {
            //Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(0, 0, 0));
            var expectedImage = new SMVImage(new Size(200, 150), ImageTypes.U8C3, new PixelVal(0, 0, 0));

            //Act
            var actualImage = image.ResizeImage(new Size(200, 150));

            //Assert
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void ShouldResizeImageFactor()
        {
            //Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(0, 0, 0));
            var expectedImage = new SMVImage(new Size(200, 150), ImageTypes.U8C3, new PixelVal(0, 0, 0));

            //Act
            var actualImage = image.ResizeImage(50);

            //Assert
            Assert.Equal(expectedImage, actualImage);
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

        [Fact]
        public void ResizeShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(0, 0, 0));

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.ResizeImage(-1));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.ResizeImage(-1, 900));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.ResizeImage(new Size(-10, 100)));
        }

        [Fact]
        public void ShouldGetOneChannel()
        {
            // Arrange
            var image = new SMVImage(new Size(156, 456), ImageTypes.U8C3, new PixelVal(120, 56, 10));
            IImageContainer[] expectedChannels = new IImageContainer[]
            {
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(120)),
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(56)),
                new SMVImage(new Size(156, 456), ImageTypes.U8C1, new PixelVal(10))
            };

            // Act
            var actualChannels = new IImageContainer[]
            {
                image.GetImageChannel(0),
                image.GetImageChannel(1),
                image.GetImageChannel(2)
            };

            // Assert
            for (int i = 0; i < expectedChannels.Length; i++)
            {
                Assert.NotNull(actualChannels[i]);
                TestHelper.BasicCompare(expectedChannels[i], actualChannels[i]);
            }
        }

        [Fact]
        public void GetImageChannelThrowArgumentOutOfRangeException()
        {
            // Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(0, 0, 0));
            var image2 = new SMVImage();

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.GetImageChannel(-1));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.GetImageChannel(4));
            Assert.Throws<System.ArgumentNullException>(() => image2.GetImageChannel(0));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.GetImageChannelClone(-1));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.GetImageChannelClone(-1));
            Assert.Throws<System.ArgumentNullException>(() => image2.GetImageChannelClone(0));
        }
      
    }


}
