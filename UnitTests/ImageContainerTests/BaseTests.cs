﻿using System;
using System.Drawing;
using ImageContainer;
using Xunit;

namespace UnitTests.ImageContainerTests
{

    /// <summary>
    /// Class that holds the base tests which can be used in another tests. Contains GetPixel, SetPixel, CreateImage and properties tests.
    /// </summary>
    public class BaseTests
    {

        [Theory]
        [InlineData(-1, -56, BorderModes.Constant, new byte[] { 15, 1, 15 })]
        [InlineData(-56, 10, BorderModes.Mirror, new byte[] { 15, 1, 15 })]
        [InlineData(-23, 0, BorderModes.BorderValue, new byte[] { 15, 1, 15 })]
        public void ShouldGetValueWithBorderPixelVal(int row, int col, BorderModes borderMode, byte[] constantValue)
        {
            //Arrange
            var image = new SMVImage();
            image.ReadImage("../../TestImages/lenaU8C3.tiff");
            PixelVal expectedPixVal = new PixelVal(15, 1, 15); ;

            if (borderMode == BorderModes.Mirror)
            {
                expectedPixVal = new PixelVal(106, 131, 224); // OpenCV position (row, col) = (1, 10)
            }
            else if (borderMode == BorderModes.BorderValue)
            {
                expectedPixVal = new PixelVal(125, 137, 226);  // OpenCV position (row, col) = (0, 0)
            }

            //Act
            var actualPixVal = image.GetPixel(row, col, borderMode, new PixelVal(constantValue));

            //Assert
            Assert.True(expectedPixVal == actualPixVal);
        }

        [Theory]
        [InlineData(0, 0, new byte[] { 125, 137, 226 })]
        [InlineData(1, 10, new byte[] { 106, 131, 224 })]
        [InlineData(54, 34, new byte[] { 97, 126, 229 })]
        [InlineData(98, 76, new byte[] { 83, 71, 173 })]
        public void ShouldGetPixelVal(int row, int col, byte[] pixelVal)
        {
            //Arrange
            var image = new SMVImage();
            image.ReadImage("../../TestImages/lenaU8C3.tiff");
            var expectedPixelVal = new PixelVal(pixelVal);

            //Act
            var actualPixVal = image.GetPixel(row, col);

            //Assert
            Assert.True(expectedPixelVal == actualPixVal);
        }

        [Fact]
        public void ShouldGetPixel()
        {
            //Arrange

            // Get all the OCV pixels to compare them with our pixel vectors
            var expPixelU8C3 = TestHelper.OCVPixelsU8C3;
            var expPixelU8C1 = TestHelper.OCVPixelsU8C1;
            var expPixelU16C3 = TestHelper.OCVPixelsU16C3;
            var expPixelU16C1 = TestHelper.OCVPixelsU16C1;
            var expPixelF32C3 = TestHelper.OCVPixelsF32C3;
            var expPixelF32C1 = TestHelper.OCVPixelsF32C1;

            // Get all test mages
            var imageU8C3 = new SMVImage();
            imageU8C3.ReadImage("../../TestImages/lenaU8C1.tiff");
            var imageU8C1 = new SMVImage();
            imageU8C1.ReadImage("../../TestImages/lenaU8C3.tiff");
            var imageU16C3 = new SMVImage();
            imageU8C3.ReadImage("../../TestImages/houseU16C3.tiff");
            var imageU16C1 = new SMVImage();
            imageU16C1.ReadImage("../../TestImages/houseU16C1.tiff");
            var imageF32C3 = new SMVImage();
            imageF32C3.ReadImage("../../TestImages/lenaF32C3.tiff");
            var imageF32C1 = new SMVImage();
            imageF32C1.ReadImage("../../TestImages/lenaF32C1.tiff");

            // Act and Assert
            for (int i = 0; i < expPixelU8C3.Length; i++)
            {
                TestHelper.CompareTwoPixelVectors<byte>(expPixelU8C3[i].Values, imageU8C3.GetPixelU8Vec(expPixelU8C3[i].Row, expPixelU8C3[i].Column));
                TestHelper.CompareTwoPixelVectors<byte>(expPixelU8C1[i].Values, imageU8C1.GetPixelU8Vec(expPixelU8C1[i].Row, expPixelU8C1[i].Column));
                TestHelper.CompareTwoPixelVectors<ushort>(expPixelU16C3[i].Values, imageU16C3.GetPixelU16Vec(expPixelU16C3[i].Row, expPixelU16C3[i].Column));
                TestHelper.CompareTwoPixelVectors<ushort>(expPixelU16C1[i].Values, imageU16C1.GetPixelU16Vec(expPixelU16C1[i].Row, expPixelU16C1[i].Column));
                TestHelper.CompareTwoPixelVectors<float>(expPixelF32C3[i].Values, imageF32C3.GetPixelF32Vec(expPixelF32C3[i].Row, expPixelF32C3[i].Column));
                TestHelper.CompareTwoPixelVectors<float>(expPixelF32C1[i].Values, imageF32C1.GetPixelF32Vec(expPixelF32C1[i].Row, expPixelF32C1[i].Column));
            }

        }

        [Fact]
        public void GetPixelShouldThrowArgumentOutOfRangeExceptionBorderThrowError()
        {
            //Arrange
            var image = new SMVImage();
            image.ReadImage("../../TestImages/lenaU8C1.tiff");

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => image.GetPixel(-34, -67, BorderModes.ThrowError));
        }

        [Fact]
        public void ShouldSetPixel()
        {
            //Arrange
            byte[] origPixel8UC3 = new byte[] { 30, 250, 35 };
            ushort[] origPixel16UC3 = new ushort[] { 10050, 4000, 65000 };
            float[] origPixel32FC3 = new float[] { 0.3f, 0.3f, 0.3f };
            byte[] exPixel8UC3 = new byte[] { 64, 128, 192 };
            ushort[] exPixel16UC3 = new ushort[] { 20000, 45678, 10000 };
            float[] exPixel32FC3 = new float[] { 0.5f, 0.7f, 1 };

            //Act and Assert U8VEC3
            var image = new SMVImage(new Size(3, 3), ImageTypes.U8C3, new PixelVal(origPixel8UC3));
            image.SetPixel(3, 3, exPixel8UC3);
            TestHelper.CompareTwoPixelVectors<byte>(exPixel8UC3, image.GetPixelU8Vec(3, 3));

            //Act and Assert U8
            image = new SMVImage(new Size(3, 3), ImageTypes.U8C1, new PixelVal(origPixel8UC3[0]));
            image.SetPixel(3, 3, exPixel8UC3[0]);
            TestHelper.CompareTwoPixelVectors<byte>(new byte[] { exPixel8UC3[0] }, image.GetPixelU8Vec(3, 3));

            //Act and Assert 16VEC3
            image = new SMVImage(new Size(3, 3), ImageTypes.U16C3, new PixelVal(origPixel16UC3));
            image.SetPixel(3, 3, exPixel16UC3);
            TestHelper.CompareTwoPixelVectors<ushort>(exPixel16UC3, image.GetPixelU16Vec(3, 3));

            //Act and Assert U16
            image = new SMVImage(new Size(3, 3), ImageTypes.U16C1, new PixelVal(origPixel16UC3[0]));
            image.SetPixel(3, 3, exPixel16UC3[0]);
            TestHelper.CompareTwoPixelVectors<ushort>(new ushort[] { exPixel16UC3[0] }, image.GetPixelU16Vec(3, 3));

            //Act and Assert F32CVEC3
            image = new SMVImage(new Size(3, 3), ImageTypes.F32C3, new PixelVal(origPixel32FC3));
            image.SetPixel(3, 3, exPixel32FC3);
            TestHelper.CompareTwoPixelVectors<float>(exPixel32FC3, image.GetPixelF32Vec(3, 3));

            //Act and Assert F32C
            image = new SMVImage(new Size(3, 3), ImageTypes.F32C1, new PixelVal(origPixel32FC3[0]));
            image.SetPixel(3, 3, exPixel32FC3[0]);
            TestHelper.CompareTwoPixelVectors<float>(new float[] { exPixel32FC3[0] }, image.GetPixelF32Vec(3, 3));
        }

        [Fact]
        public void SetShouldThrowArgumentException()
        {
            // Arrange
            var image = new SMVImage(new Size(10, 10), ImageTypes.U8C3, new PixelVal(120, 130, 140));
            var pixelValObjToSet = new PixelVal(new double[] { 10.6, 20.2, 0.6 });
            var pixelvalArrayToSet = new byte[] { 10, 20 };

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetPixel(10, 10, pixelValObjToSet)); // Bad type of PixelType. Double vs byte
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetPixel(10, 10, pixelvalArrayToSet)); // Number of elems in array does not match channels in image
        }

        [Fact]
        public void SetShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            var image = new SMVImage(new Size(400, 300), ImageTypes.U8C3, new PixelVal(120, 130, 140));

            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetPixel(-10, 90, 90));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => image.SetPixel(10, image.Width + 10, 90));
        }


        [Fact]
        public void SetShouldThrowOverflowException()
        {
            // Arrange
            var image = new SMVImage(new Size(50, 50), ImageTypes.U8C3, new PixelVal(120, 130, 140));
            var image2 = new SMVImage(new Size(50, 50), ImageTypes.U16C1, 65000);
            var image3 = new SMVImage(new Size(50, 50), ImageTypes.F32C1, 0.2f);

            // Assert
            Assert.Throws<System.OverflowException>(() => image.SetPixel(10, 35, 400));
            Assert.Throws<System.OverflowException>(() => image2.SetPixel(10, 10, 90000));
            Assert.Throws<System.OverflowException>(() => image3.SetPixel(10, 10, 1.1f));
        }

        // This test actually tests the CreateNewImage function because the constructor calls it internally
        [Fact]
        public void ShouldCreateImageFromImageType()
        {
            // Arrange
            var expImageSize = new Size(120, 120);
            byte[] expPixelU8C3 = new byte[] { 120, 34, 90 };
            ushort[] expPixelU16C3 = new ushort[] { 4500, 2341, 65000 };
            float[] expPixelF32C3 = new float[] { 0.2f, 0.5f, 0.1f };

            //Act and Assert U8C1
            var actualImage = new SMVImage(expImageSize, ImageTypes.U8C1, 120);
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(1, actualImage.Channels);
            Assert.Equal(ImageTypes.U8C1, actualImage.Type);
            Assert.Equal(120, actualImage.GetPixelU8(34, 12));

            //Act and Assert U8C3
            actualImage = new SMVImage(expImageSize, ImageTypes.U8C3, new PixelVal(expPixelU8C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.U8C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<byte>(expPixelU8C3, actualImage.GetPixelU8Vec(120, 20));

            //Act and Assert U16C3
            actualImage = new SMVImage(expImageSize, ImageTypes.U16C3, new PixelVal(expPixelU16C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.U16C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<ushort>(expPixelU16C3, actualImage.GetPixelU16Vec(120, 20));

            //Act and Assert U32C3
            actualImage = new SMVImage(120, 120, ImageTypes.F32C3, new PixelVal(expPixelF32C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.F32C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<float>(expPixelF32C3, actualImage.GetPixelF32Vec(120, 20));
        }

        [Fact]
        public void ShouldCreateImageFromPixelTypeAndChannels()
        {
            // Arrange
            var expImageSize = new Size(120, 120);
            byte[] expPixelU8C3 = new byte[] { 120, 34, 90 };
            ushort[] expPixelU16C3 = new ushort[] { 4500, 2341, 65000 };
            float[] expPixelF32C3 = new float[] { 0.2f, 0.5f, 0.1f };

            //Act and Assert U8C1
            var actualImage = new SMVImage(expImageSize, PixelTypes.U8, 1, 120);
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(1, actualImage.Channels);
            Assert.Equal(ImageTypes.U8C1, actualImage.Type);
            Assert.Equal(120, actualImage.GetPixelU8(34, 12));

            //Act and Assert U8C3
            actualImage = new SMVImage(expImageSize, PixelTypes.U8, 3, new PixelVal(expPixelU8C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.U8C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<byte>(expPixelU8C3, actualImage.GetPixelU8Vec(120, 20));

            //Act and Assert U16C3
            actualImage = new SMVImage(expImageSize, PixelTypes.U16, 3, new PixelVal(expPixelU16C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.U16C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<ushort>(expPixelU16C3, actualImage.GetPixelU16Vec(120, 20));

            //Act and Assert U32C3
            actualImage = new SMVImage(expImageSize, PixelTypes.F32, 3, new PixelVal(expPixelF32C3));
            Assert.Equal(expImageSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(3, actualImage.Channels);
            Assert.Equal(ImageTypes.F32C3, actualImage.Type);
            TestHelper.CompareTwoPixelVectors<float>(expPixelF32C3, actualImage.GetPixelF32Vec(120, 20));
        }

        [Fact]
        public void CreateImageShouldThrowArgumentOutOfRangeException()
        {
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => new SMVImage(120, 120, PixelTypes.U8, 10, 120));
            Assert.Throws<ArgumentOutOfRangeException>(() => new SMVImage(120, 120, PixelTypes.U16, 6, 120));
            Assert.Throws<ArgumentOutOfRangeException>(() => new SMVImage(new Size(120, 10), PixelTypes.F32, 7, 120));
        }

        [Theory]
        [InlineData(890, 900)]
        [InlineData(320, 800)]
        [InlineData(20, 100)]
        public void ShouldGetImageHeigthWidth(int height, int width)
        {
            // Arrange
            var image = new SMVImage(new Size(width, height), ImageTypes.U8C1, new PixelVal(0));
            int expectedHeight = height;
            int expectedWidth = width;

            // Act
            int actualHeight = image.Height;
            int actualWidth = image.Width;

            // Assert
            Assert.Equal(expectedHeight, actualHeight);
            Assert.Equal(expectedWidth, actualWidth);
        }

        [Theory]
        [InlineData(156, 456, 71136)]
        [InlineData(678, 587, 397986)]
        public void ShouldGetImageTotalPixels(int height, int width, long totalPixels)
        {
            // Arrange
            var image = new SMVImage(new Size(width, height), ImageTypes.U8C1, new PixelVal(0));
            long expectedTotalPixels = totalPixels;

            // Act
            long actualTotalPixels = image.TotalPixels;

            // Assert
            Assert.Equal(expectedTotalPixels, actualTotalPixels);
        }

        [Theory]
        [InlineData(ImageTypes.F32C3, 3)]
        [InlineData(ImageTypes.U16C5, 5)]
        [InlineData(ImageTypes.U8C4, 4)]
        [InlineData(ImageTypes.U16C1, 1)]
        [InlineData(ImageTypes.F32C2, 2)]
        public void ShouldGetImageChannels(ImageTypes imageType, int channels)
        {
            // Arrange
            var image = new SMVImage(new Size(156, 456), imageType, new PixelVal(0));
            int expectedChannels = channels;

            // Act
            int actualChannels = image.Channels;

            // Assert
            Assert.Equal(expectedChannels, actualChannels);
        }

        [Theory]
        [InlineData(ImageTypes.F32C3, 3)]
        [InlineData(ImageTypes.U16C5, 5)]
        [InlineData(ImageTypes.U8C4, 4)]
        [InlineData(ImageTypes.U16C1, 1)]
        [InlineData(ImageTypes.F32C2, 2)]
        public void ShouldGetImageType(ImageTypes imageType, int channels)
        {
            // Arrange
            var image = new SMVImage(new Size(156, 456), imageType, new PixelVal(0));
            int expectedChannels = channels;
            ImageTypes expectedImageType = imageType;

            // Act
            int actualChannels = image.Channels;
            ImageTypes actualImageType = image.Type;

            // Assert
            Assert.Equal(expectedImageType, actualImageType);
            Assert.Equal(expectedChannels, actualChannels);
        }

        [Theory]
        [InlineData(ImageTypes.F32C3, PixelTypes.F32)]
        [InlineData(ImageTypes.U16C5, PixelTypes.U16)]
        [InlineData(ImageTypes.U8C4, PixelTypes.U8)]
        public void ShouldGetPixelType(ImageTypes imageType, PixelTypes pixelType)
        {
            // Arrange
            var image = new SMVImage(new Size(156, 456), imageType, new PixelVal(0));
            PixelTypes expectedPixelType = pixelType;

            // Act
            PixelTypes actualPixelType = image.PixelType;

            // Assert
            Assert.Equal(expectedPixelType, actualPixelType);
        }


    }
}
