﻿using Xunit;
using ImageContainer;
using System.Drawing;

namespace UnitTests.IOManagerTests
{
    public class IOManagerTests
    {
        [Fact]
        public void ReadImageShouldThrowFileNotFoundException()
        {
            // Assert
            Assert.Throws<System.IO.FileNotFoundException>(() => new SMVImage().ReadImage("image_not_exist.png"));
            Assert.Throws<System.IO.FileNotFoundException>(() => new SMVImage().ReadImage("image_not_exist.axs", new Rectangle(30, 30, 70, 70)));
            Assert.Throws<System.IO.FileNotFoundException>(() => new SMVImage().ReadImage("image_not_exist.fas", 30, 30, 100, 100));
        }

        [Fact]
        public void ReadImageShouldThrowImageFormatNotSupportedException()
        {
            // Assert
            Assert.Throws<ImageFormatNotSupportedException>(() => new SMVImage().ReadImage("../../TestImages/psdImage.psd", 30, 30, 100, 100));
            Assert.Throws<ImageFormatNotSupportedException>(() => new SMVImage().ReadImage("../../TestImages/psdImage.psd", new Rectangle(30, 30, 70, 70)));
            Assert.Throws<ImageFormatNotSupportedException>(() => new SMVImage().ReadImage("../../TestImages/psdImage.psd", 30, 30, 100, 100));
        }

        [Fact]
        public void ReadImageROIShouldThrowArgumentOutOfRangeException()
        {
            // Assert
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new SMVImage().ReadImage("../../TestImages/lenaU8C1.tiff", new Rectangle(30, 30, 7000, 70), BorderModes.ThrowError));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new SMVImage().ReadImage("../../TestImages/psdImage.psd", -30, 30, 100, 100, BorderModes.ThrowError));
        }

        [Fact]
        public void ShouldReadFromFileAndCreateImageObject()
        {
            // Arrange
            ImageTypes expectedImgType = ImageTypes.U8C3;
            Size expectedSize = new Size(512, 512);
            int expectedChannels = 3;
            long expectedRowSteps = 1536;

            // Act and Assert
            var actualImage = new SMVImage();
            actualImage.ReadImage("../../TestImages/lenaU8C3.tiff");
            
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImgType, actualImage.Type);
            Assert.Equal(expectedChannels, actualImage.Channels);
            Assert.Equal(expectedSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(expectedRowSteps, actualImage.Steps);
        }

        [Fact]
        public void ShouldReadFromFileAndCreateImageObjectWithROI()
        {
            // Arrange
            var rectRoi = new Rectangle(200, 200, 200, 200);
            var wholeImage = new SMVImage();
            wholeImage.ReadImage("../../TestImages/lenaU8C3.tiff");
            ImageTypes expectedImgType = ImageTypes.U8C3;
            Size expectedSize = new Size(200, 200);
            int expectedChannels = 3;
            long expectedRowSteps = 1536;

            // Act and Assert
            var actualImage = new SMVImage();
            actualImage.ReadImage("../../TestImages/lenaU8C3.tiff", rectRoi);
           
            Assert.NotNull(wholeImage);
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImgType, actualImage.Type);
            Assert.Equal(expectedChannels, actualImage.Channels);
            Assert.Equal(expectedSize, new Size(actualImage.Width, actualImage.Height));
            Assert.Equal(expectedRowSteps, actualImage.Steps);
            TestHelper.CheckRoiInImage(wholeImage, actualImage, rectRoi);
        }

        [Fact]
        public void SaveImageShouldThrowArgumentNullException()
        {
            //Arrange
            string url = null;

            // Assert
            Assert.Throws<System.ArgumentNullException>(() => new SMVImage().SaveImage(url));
        }

        [Theory]
        [InlineData(ImageTypes.U16C5, new double[] { 2343, 4343, 1323, 53545, 5565 })]
        [InlineData(ImageTypes.U8C5, new double[] { 23, 56, 123, 234, 12 })]
        [InlineData(ImageTypes.F32C5, new double[] { 0.4, 0.1, 0.8, 0.1, 0.9 })]
        public void SaveImageShouldThrowException(ImageTypes imageType, double[] pixelVals)
        {
            //Arrange
            var image = new SMVImage(new Size(156, 456), imageType, new PixelVal(pixelVals));

            // Assert
            Assert.Throws<System.Exception>(() => image.SaveImage("imageToSave.png"));
        }

        [Fact]
        public void ShouldSaveFromImageToFile()
        {
            // Arrange
            string imageUrl = "imageToSave.png";
            var expectedImage = new SMVImage(new Size(156, 456), ImageTypes.U8C3, new PixelVal(24, 120, 90));
            ImageTypes expectedImgType = ImageTypes.U8C3;
            Size expectedSize = new Size(200, 200);
            int expectedChannels = 3;
            IImageContainer actualImage = null;

            // Act
            expectedImage.SaveImage(imageUrl);

            if (System.IO.File.Exists(imageUrl)) {
                actualImage.ReadImage(imageUrl);
            }

            // Assert
            Assert.NotNull(actualImage);
            Assert.Equal(expectedImgType, actualImage.Type);
            Assert.Equal(expectedChannels, actualImage.Channels);
            Assert.Equal(expectedSize, new Size(actualImage.Width, actualImage.Height));
            TestHelper.BasicCompare(expectedImage, actualImage);
        }

    }
}
