﻿using System;
using System.Drawing;
using ImageContainer;
using Xunit;

namespace UnitTests
{
    /// <summary>
    /// Useful functions to help to run tests.
    /// </summary>
    public static class TestHelper
    {

        #region Constant OpenCV pixel values

        // Values of real images to use in tests. These values have been got from OpenCV.
        // These values serve to be compared with the ones we will get from our GetPixel functions

        public static readonly OCVTestPixel<byte>[] OCVPixelsU8C3 = new OCVTestPixel<byte>[]
        {
            new OCVTestPixel<byte> { Row = 0, Column = 0, Values =  new byte[] { 125, 137, 226 } },
            new OCVTestPixel<byte> { Row = 10, Column = 1, Values =  new byte[] { 106, 131, 224 } },
            new OCVTestPixel<byte> { Row = 54, Column = 34, Values =  new byte[] { 99, 129, 229 } },
            new OCVTestPixel<byte> { Row = 98, Column = 76, Values =  new byte[] { 78, 65, 163 } }
        };

        public static readonly OCVTestPixel<byte>[] OCVPixelsU8C1 = new OCVTestPixel<byte>[]
        {
            new OCVTestPixel<byte> { Row = 0, Column = 0, Values =  new byte[] { 169 } },
            new OCVTestPixel<byte> { Row = 10, Column = 1, Values =  new byte[] { 164 } },
            new OCVTestPixel<byte> { Row = 54, Column = 34, Values =  new byte[] { 164 } },
            new OCVTestPixel<byte> { Row = 98, Column = 76, Values =  new byte[] { 107 } }
        };

        public static readonly OCVTestPixel<ushort>[] OCVPixelsU16C3 = new OCVTestPixel<ushort>[]
        {
            new OCVTestPixel<ushort> { Row = 0, Column = 0, Values =  new ushort[] { 6692, 7625, 8287 } },
            new OCVTestPixel<ushort> { Row = 10, Column = 1, Values =  new ushort[] { 6322, 7655, 8027 } },
            new OCVTestPixel<ushort> { Row = 54, Column = 34, Values =  new ushort[] { 6543, 7987, 8255 } },
            new OCVTestPixel<ushort> { Row = 98, Column = 76, Values =  new ushort[] { 6960, 8089, 8522 } }
        };

        public static readonly OCVTestPixel<ushort>[] OCVPixelsU16C1 = new OCVTestPixel<ushort>[]
        {
            new OCVTestPixel<ushort> { Row = 0, Column = 0, Values =  new ushort[] { 7717 } },
            new OCVTestPixel<ushort> { Row = 10, Column = 1, Values =  new ushort[] { 7614 } },
            new OCVTestPixel<ushort> { Row = 54, Column = 34, Values =  new ushort[] { 7903 } },
            new OCVTestPixel<ushort> { Row = 98, Column = 76, Values =  new ushort[] { 8090 } }
        };

        public static readonly OCVTestPixel<float>[] OCVPixelsF32C3 = new OCVTestPixel<float>[]
        {
            new OCVTestPixel<float> { Row = 0, Column = 0, Values =  new float[] { 0.2158606f, 0.2541521f, 0.7529425f } },
            new OCVTestPixel<float> { Row = 10, Column = 1, Values =  new float[] { 0.1499598f, 0.226966f, 0.7379106f } },
            new OCVTestPixel<float> { Row = 54, Column = 34, Values =  new float[] { 0.1247719f, 0.2195263f, 0.7912982f } },
            new OCVTestPixel<float> { Row = 98, Column = 76, Values =  new float[] { 0.07818747f, 0.04817183f, 0.3762622f } }
        };

        public static readonly OCVTestPixel<float>[] OCVPixelsF32C1 = new OCVTestPixel<float>[]
        {
            new OCVTestPixel<float> { Row = 0, Column = 0, Values =  new float[] { 0.3989252f } },
            new OCVTestPixel<float> { Row = 10, Column = 1, Values =  new float[] { 0.3709597f } },
            new OCVTestPixel<float> { Row = 54, Column = 34, Values =  new float[] { 0.3796841f } },
            new OCVTestPixel<float> { Row = 98, Column = 76, Values =  new float[] { 0.1496926f } }
        };

        #endregion

        /// <summary>
        /// Compares and asserts if some values between two matrices are equal
        /// </summary>
        /// <param name="image1">Original image</param>
        /// <param name="image2">Result image</param>
        /// <param name="yStepPercent">Percent of height to get the traversal steps. Default 20%</param>
        /// <param name="xStepPercent">Percent of width to get the traversal steps. Default 20%</param>
        public static void BasicCompare(IImageContainer image1,
            IImageContainer image2,
            float yStepPercent = 0.2f,
            float xStepPercent = 0.2f)
        {

            // Arrange
            // Choose pixels at every % of Height/Width steps to not travers all the matrix
            // 1/Height and 1/Width to get a complete traversal
            int stepsY = (int)Math.Round(image1.Height * yStepPercent);
            int stepsX = (int)Math.Round(image1.Width * xStepPercent);

            // Assert
            Assert.Equal(image1.GetType(), image2.GetType());

            for (int y = 0; y < image1.Height; y += stepsY)
            {
                for (int x = 0; x < image1.Width; x += stepsX)
                {
                    var expectedPixelVal = image1.GetPixel(y, x);
                    var actualPixelVal = image2.GetPixel(y, x);

                    for (int i = 0; i < image1.Channels; i++)
                    {
                        Assert.Equal(expectedPixelVal[i], actualPixelVal[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Compares and asserts if some values between a matrix and a color set are equal
        /// </summary>
        /// <param name="image1">Original image</param>
        /// <param name="pixelVal">Color set to compare with</param>
        /// <param name="yStepPercent">Percent of height to get the traversal steps. Default 20%</param>
        /// <param name="xStepPercent">Percent of width to get the traversal steps. Default 20%</param>
        public static void BasicCompare(IImageContainer image1,
           PixelVal pixelVal,
           float yStepPercent = 0.2f,
           float xStepPercent = 0.2f)
        {

            // Arrange
            // Choose pixels at every % of Height/Width steps to not travers all the matrix
            // 1/Height and 1/Width to get a complete traversal
            int stepsY = (int)Math.Round(image1.Height * yStepPercent);
            int stepsX = (int)Math.Round(image1.Width * xStepPercent);

            // Assert
            for (int y = 0; y < image1.Height; y += stepsY)
            {
                for (int x = 0; x < image1.Width; x += stepsX)
                {
                    var expectedPixelVal = image1.GetPixel(y, x);

                    for (int i = 0; i < image1.Channels; i++)
                    {
                        Assert.Equal(expectedPixelVal[i], pixelVal[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Asserts if a ROI is inside an image
        /// </summary>
        /// <param name="origImage">Original image</param>
        /// <param name="roiImage">Result sub-image</param>
        /// <param name="roiExpected">Rectangle that represents the original ROI to extract from the original image</param>
        public static void CheckRoiInImage(IImageContainer origImage, IImageContainer roiImage, Rectangle roiExpected)
        {
            // Arrange
            int yStart = roiExpected.X;
            int xStart = roiExpected.Y;
            int yEnd = roiExpected.X + roiExpected.Width;
            int xEnd = roiExpected.Y + roiExpected.Height;

            int yRoi = 0;
            int xRoi = 0;

            // Assert
            for (int y = yStart; y <= yEnd; y++)
            {
                for (int x = xStart; x <= xEnd; x++)
                {
                    var expectedPixelVal = origImage.GetPixel(y, x);
                    var actualPixelVal = roiImage.GetPixel(yRoi, xRoi);

                    Assert.True(expectedPixelVal == actualPixelVal);
                    xRoi++;
                }
                yRoi++;
            }
        }

        /// <summary>
        /// Compares two arrays of values of the same type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pixelVec1">Array of values of the first pixel.</param>
        /// <param name="pixelVec2">Array of values of the second pixel.</param>
        public static void CompareTwoPixelVectors<T>(T[] pixelVec1, T[] pixelVec2)
        {
            if (pixelVec1.Length != pixelVec2.Length)
            {
                throw new ArgumentException("Two vectors have to have same length.");
            }

            for (int i = 0; i < 3; i++)
            {
                Assert.Equal(pixelVec1[i], pixelVec2[i]);
            }
        }

    }
}
