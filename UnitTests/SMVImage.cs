﻿using System;
using System.Drawing;
using ImageContainer;

namespace UnitTests
{
    public class SMVImage : IImageContainer
    {
        #region Image properties

        public int Height { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Width { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public long TotalPixels { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Channels { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ImageTypes Type { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public PixelTypes PixelType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public long Steps { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        long IImageContainer.Steps => throw new NotImplementedException();

        #endregion

        #region Constructors

        public SMVImage()
        {

        }

        public SMVImage(int height, int width, ImageTypes imageType, PixelVal value)
        {
            CreateImage(new Size(width, height), imageType, value);
        }

        public SMVImage(int height, int width, ImageTypes imageType, byte value)
        {
            CreateImage(height, width, imageType, value);
        }

        public SMVImage(int height, int width, ImageTypes imageType, ushort value)
        {
            CreateImage(height, width, imageType, value);
        }

        public SMVImage(int height, int width, ImageTypes imageType, int value)
        {
            CreateImage(height, width, imageType, value);
        }

        public SMVImage(int height, int width, ImageTypes imageType, float value)
        {
            CreateImage(height, width, imageType, value);
        }

        public SMVImage(int height, int width, ImageTypes imageType, double value)
        {
            CreateImage(height, width, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, byte value)
        {
            CreateImage(size, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, ushort value)
        {
            CreateImage(size, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, int value)
        {
            CreateImage(size, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, float value)
        {
            CreateImage(size, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, double value)
        {
            CreateImage(size, imageType, value);
        }

        public SMVImage(Size size, ImageTypes imageType, PixelVal value)
        {
            CreateImage(size, imageType, value);
        }

        // PixelType and number of channels

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, PixelVal value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, byte value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, ushort value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, int value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, float value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(int height, int width, PixelTypes pixelType, int noOfChannels, double value)
        {
            CreateImage(new Size(width, height), pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, byte value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, ushort value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, int value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, float value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, double value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        public SMVImage(Size size, PixelTypes pixelType, int noOfChannels, PixelVal value)
        {
            CreateImage(size, pixelType, noOfChannels, value);
        }

        #endregion

        #region Image functions
        public IImageContainer CopyImageRef()
        {
            throw new NotImplementedException();
        }

        public IImageContainer CloneImage()
        {
            throw new NotImplementedException();
        }

        public IImageContainer ConvertToGrayScale()
        {
            throw new NotImplementedException();
        }

        public IImageContainer ResizeImage(int newHeight, int newWidth, InterpolationModes interpolationModes = InterpolationModes.Bilinear)
        {
            throw new NotImplementedException();
        }

        public IImageContainer ResizeImage(Size newSize, InterpolationModes interpolationModes = InterpolationModes.Bilinear)
        {
            throw new NotImplementedException();
        }

        public IImageContainer ResizeImage(float factor, InterpolationModes interpolationModes = InterpolationModes.Bilinear)
        {
            throw new NotImplementedException();
        }

        public void SetTo(PixelVal value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(byte value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(ushort value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(int value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(float value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(double value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(byte[] value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(ushort[] value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(int[] value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(float[] value)
        {
            throw new NotImplementedException();
        }

        public void SetTo(double[] value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, PixelVal value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, byte value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, ushort value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, int value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, float value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, double value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, byte[] value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, ushort[] value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, int[] value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, float[] value)
        {
            throw new NotImplementedException();
        }

        public void SetPixel(int row, int col, double[] value)
        {
            throw new NotImplementedException();
        }

        public PixelVal GetPixel(int row, int col, BorderModes borderMode = BorderModes.ThrowError, PixelVal constantValue = null)
        {
            throw new NotImplementedException();
        }

        public byte GetPixelU8(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, byte constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPixelU8Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, byte constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public ushort GetPixelU16(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, ushort constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public ushort[] GetPixelU16Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, ushort constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public int GetPixelI32(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, int constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public int[] GetPixelI32Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, int constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public float GetPixelF32(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, float constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public float[] GetPixelF32Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, float constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public double GetPixelF64(int row, int col, int channel = 0, BorderModes borderMode = BorderModes.ThrowError, double constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public double[] GetPixelF64Vec(int row, int col, BorderModes borderMode = BorderModes.ThrowError, double constantValue = 0)
        {
            throw new NotImplementedException();
        }

        public dynamic GetPixelVal(int row, int col, BorderModes borderMode = BorderModes.ThrowError, dynamic constantValue = null)
        {
            throw new NotImplementedException();
        }

        public IImageContainer SubImage(Rectangle rectROI)
        {
            throw new NotImplementedException();
        }

        public IImageContainer SubImage(int row1, int col1, int row2, int col2)
        {
            throw new NotImplementedException();
        }

        public IImageContainer SubImageClone(Rectangle rectROI)
        {
            throw new NotImplementedException();
        }

        public IImageContainer SubImageClone(int row1, int col1, int row2, int col2)
        {
            throw new NotImplementedException();
        }

        public IImageContainer[] Split()
        {
            throw new NotImplementedException();
        }

        public void ConvertToType(PixelTypes newPixelType)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, byte value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, ushort value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, int value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, float value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, double value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, ImageTypes imageType, PixelVal value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, byte value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, ushort value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, int value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, float value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, double value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, ImageTypes imageType, PixelVal value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, byte value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, ushort value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, int value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, float value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, double value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(Size size, PixelTypes pixelType, int noOfChannels, PixelVal value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, byte value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, ushort value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, int value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, float value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, double value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer CreateImage(int sizeX, int sizeY, PixelTypes pixelType, int noOfChannels, PixelVal value)
        {
            throw new NotImplementedException();
        }

        public IImageContainer GetImageChannel(Channels channel)
        {
            throw new NotImplementedException();
        }

        public IImageContainer GetImageChannel(int channel)
        {
            throw new NotImplementedException();
        }

        public IImageContainer GetImageChannelClone(int channel)
        {
            throw new NotImplementedException();
        }

        public IImageContainer GetImageChannelClone(Channels channel)
        {
            throw new NotImplementedException();
        }

        public void ReadImage(string url, ImageModes imageMode = ImageModes.Original)
        {
            throw new NotImplementedException();
        }

        public void ReadImage(string url, Rectangle rectROI, ImageModes imageMode = ImageModes.Original)
        {
            throw new NotImplementedException();
        }

        public void ReadImage(string url, int x1, int y1, int x2, int y2, ImageModes imageMode = ImageModes.Original)
        {
            throw new NotImplementedException();
        }

        public void SaveImage(string url, ImageFileTypes imageFile = ImageFileTypes.Bmp)
        {
            throw new NotImplementedException();
        }

        public void ReadImage(string url, Rectangle rectROI, BorderModes borderMode, dynamic constantValue = null, ImageModes imageMode = ImageModes.Original)
        {
            throw new NotImplementedException();
        }

        public void ReadImage(string url, int x1, int y1, int x2, int y2, BorderModes borderMode, dynamic constantValue = null, ImageModes imageMode = ImageModes.Original)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
